print("hola")
import pandas as pd

import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_validate

from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier

from sklearn.metrics import accuracy_score


X_train_data = pd.read_csv('~/Escritorio/UCIHARDataset/train/X_train.txt', delim_whitespace=True, header=None)
Y_train_data = pd.read_csv('~/Escritorio/UCIHARDataset/train/y_train.txt', delim_whitespace=True, header=None)

X_test_data = pd.read_csv('~/Escritorio/UCIHARDataset/test/X_test.txt', delim_whitespace=True, header=None)
Y_test_data = pd.read_csv('~/Escritorio/UCIHARDataset/test/y_test.txt', delim_whitespace=True, header=None)

labels= Y_train_data.values.ravel()
print(X_train_data.shape)
print(Y_train_data.shape)
print(labels)


#Random_Forest_model = RandomForestClassifier(n_estimators=100,criterion="entropy")
Random_Forest_model = KNeighborsClassifier(n_neighbors=5, weights="uniform", algorithm="auto", leaf_size=30, p=2, metric="minkowski")

# 	#Cross validation
# 	#accuracy = cross_validate(Random_Forest_model,X_train_data,labels,cv=10)['test_score']
# 	#print(accuracy)
# 	# print('The accuracy is: ',sum(accuracy)/len(accuracy)*100,'%')

Random_Forest_model.fit(X_train_data, labels)
resultado=Random_Forest_model.predict(X_train_data)
# 	#hacer resultado con el test data

score=Random_Forest_model.score(X_test_data, Y_test_data)
print(score)
# score=np.mean(np.array(resultado)==np.array(labels))
# print(score)


# resultado2=Random_Forest_model.predict(X_test_data)
# score2=np.mean(np.array(resultado2)==np.array(Y_test_data))
# print(score2)


################# KNN ###################

# nbrs = NearestNeighbors(n_neighbors=2, algorithm='ball_tree').fit(X_train_data)
# distances, indices = nbrs.kneighbors(X_train_data)
# print (indices)
# print(distances)


################### MLP ###################

# clf = MLPClassifier(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(5, 2), random_state=1)
# clf.fit(X_train_data, labels) 
# resultado_mlp=clf.predict(X_test_data)
# print(resultado_mlp)
# score=clf.score(X_test_data, Y_test_data)
# print(score)

